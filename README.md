# Runing the application

If you have the right credentials, you can download [Expo](https://expo.io) and load the published app from the projects dashboard. Otherwise install the dependecies, run `expo start` and choose open in android/ios simulator in the dev menu.

# Demo

![Demo-Gif](/docs/assets/demo.gif)

# Screens

## Empty State

![Empty-State](docs/assets/empty-state.jpg)

## Error State

![Error-State](docs/assets/database-error.jpg)

## Loading

![Loading-State](docs/assets/loading-state.jpg)

## Picture List

![List](/docs/assets/list.jpg)

## Picture

![List](/docs/assets/picture.jpg)
