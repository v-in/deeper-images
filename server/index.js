const express = require("express");
const multer = require("multer");
const fs = require("fs").promises;
const path = require("path");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    const filename = req.body.filename;
    cb(null, filename);
  }
});

const upload = multer({ storage });

const app = express();

app.post("/images", upload.single("file"), (req, res) => {
  res.status(200).json({ file: req.file });
});

app.get("/images", async (req, res) => {
  let fileNames;
  try {
    fileNames = await fs.readdir("uploads");
  } catch (e) {
    fileNames = [];
  }
  const stats = await Promise.all(
    fileNames.map(f => fs.stat(path.resolve("uploads", f)))
  );
  const data = fileNames
    .map((f, idx) => ({
      fileName: f,
      createdAt: stats[idx].birthtime,
      size: stats[idx].size / 1000000
    }))
    .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));

  res.json(data);
});

app.get("/images/:file_name", (req, res) => {
  res.setHeader("Content-Type", "image/jpg");
  res.sendFile(path.resolve("uploads", req.params.file_name));
});

app.listen(3000, "0.0.0.0", () => {
  console.log("Listening on port 3000");
});
