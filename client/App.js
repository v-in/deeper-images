import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import PictureList from "./views/PictureList";
import NewPictureButton from "./views/PictureList/NewPictureButton";
import { SWRConfig } from "swr";
import { API_URL } from "./constants";
import NewPicture from "./views/NewPicture/NewPicture";
import { ViewPicture } from "./views/ViewPicture";

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <SWRConfig
        value={{
          fetcher: path => {
            const url = API_URL + path;
            return fetch(url)
              .then(res => res.json())
              .then(data => {
                return data;
              });
          },
          refreshInterval: 3000,
          errorRetryCount: 2,
          errorRetryInterval: 3000
        }}
      >
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              height: 80
            },
            headerRightContainerStyle: {
              paddingRight: 20
            },
            headerLeftContainerStyle: {
              paddingLeft: 8
            },
            headerTitleAlign: "center"
          }}
        >
          <Stack.Screen
            name="Pictures"
            component={PictureList}
            options={{
              headerRight: props => <NewPictureButton {...props} />
            }}
          />
          <Stack.Screen
            name="New Picture"
            component={NewPicture}
            options={{ headerBackTitle: "Back" }}
          />
          <Stack.Screen
            name="View Picture"
            component={ViewPicture}
            options={{ headerBackTitle: "Back" }}
          />
        </Stack.Navigator>
      </SWRConfig>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default App;
