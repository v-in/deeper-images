import { useEffect } from "react";
import * as ImagePicker from "expo-image-picker";

const useCameraPermissions = () => {
  useEffect(() => {
    ImagePicker.requestCameraPermissionsAsync().then(({ status }) => {
      if (status !== "granted")
        alert("Sorry, we need camera permissions to make this work!");
    });
  }, []);
};

export default useCameraPermissions;
