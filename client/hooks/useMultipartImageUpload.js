const useMultipartImageUpload = () => {
  const startUpload = (endpoint, image_uri, filename) => {
    const form = new FormData();

    form.append("filename", filename);
    form.append("file", {
      uri: image_uri,
      type: "image/jpeg",
      name: "teste.jpg"
    });

    return fetch(endpoint, {
      method: "POST",
      body: form,
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
  };

  return {
    startUpload
  };
};

export default useMultipartImageUpload;
