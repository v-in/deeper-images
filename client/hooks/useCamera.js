import useCameraPermissions from "./useCameraPermissions";
import * as ImagePicker from "expo-image-picker";

const useCamera = () => {
  useCameraPermissions();

  const takePicture = () =>
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      quality: 1
    });

  return {
    takePicture
  };
};

export default useCamera;
