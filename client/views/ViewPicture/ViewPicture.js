import React, { useState } from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import * as Progress from "react-native-progress";
import { iOSColors } from "react-native-typography";
import { Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

const dimensions = Dimensions.get("window");

const ViewPicture = ({ route }) => {
  const [loading, setLoading] = useState(false);

  const onLoadStart = () => setLoading(true);
  const onLoadEnd = () => setLoading(false);

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{ flexGrow: 1, backgroundColor: "white" }}
    >
      {loading && (
        <Progress.Bar
          indeterminate
          color={iOSColors.blue}
          width={null}
          borderRadius={0}
          height={2}
          borderWidth={0}
        />
      )}
      <View
        style={{
          justifyContent: "flex-start",
          alignItems: "center"
        }}
      >
        <Image
          source={{ uri: route.params.uri }}
          style={{
            width: dimensions.width,
            height: dimensions.height,
            resizeMode: "contain"
          }}
          onLoadStart={onLoadStart}
          onLoadEnd={onLoadEnd}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  image: {
    width: dimensions.width,
    minHeight: 200
  },
  container: {
    backgroundColor: "white"
  }
});

export default ViewPicture;
