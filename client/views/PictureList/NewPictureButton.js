import React, { useState } from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { iOSColors } from "react-native-typography";
import { useNavigation } from "@react-navigation/core";

const NewPictureButton = () => {
  const navigation = useNavigation();

  const onPress = () => {
    navigation.navigate("New Picture");
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <Ionicons
        name="ios-add-circle-outline"
        size={30}
        color={iOSColors.blue}
      />
    </TouchableOpacity>
  );
};

export default NewPictureButton;
