import React from "react";
import {
  FlatList,
  View,
  StyleSheet,
  Text,
  Image,
  RefreshControl
} from "react-native";
import PictureItem, { TOTAL_PICTURE_ROW_HEIGHT } from "./PictureItem";
import useSWR from "swr";
import { API_URL } from "../../constants";
import { iOSUIKit, iOSColors } from "react-native-typography";
import * as Progress from "react-native-progress";

const State = ({ type }) => {
  const emptyImage = require("../../assets/empty-box.png");
  const databaseError = require("../../assets/database-error.png");
  let image;

  if (type === "loading")
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <Progress.Bar
          indeterminate
          color={iOSColors.blue}
          width={null}
          borderRadius={0}
          height={2}
          borderWidth={0}
        />
      </View>
    );

  if (type === "empty") image = emptyImage;
  if (type === "error") image = databaseError;

  return (
    <View style={styles.emptyStateContainer}>
      <View style={{ width: 100, height: 100, marginBottom: 10 }}>
        <Image style={styles.emptyStateImage} source={image}></Image>
      </View>
      <Text style={{ ...iOSUIKit.footnoteObject, color: iOSColors.gray }}>
        {type === "empty"
          ? "Looks like you haven't uploaded any pictures!"
          : "Oops, couldn't connect to database."}
      </Text>
    </View>
  );
};

const PictureList = () => {
  const { data, error, mutate } = useSWR("/images");

  const onRefresh = () => mutate(undefined, true);
  const renderItem = ({ item }) => <PictureItem {...item} />;
  const keyExtractor = item => item.fileName;
  //  We can implement getItemLayout as a performance optimization
  // because row height is known and constant
  const getItemLayout = (data, index) => ({
    length: TOTAL_PICTURE_ROW_HEIGHT,
    offset: TOTAL_PICTURE_ROW_HEIGHT * index,
    index
  });

  if (error) return <State type="error" />;

  if (!data) return <State type="loading" />;

  if (data.length === 0) return <State type="empty" />;

  const dataWithUris = data.map(elem => {
    const uri = API_URL + "/images/" + elem.fileName;
    return {
      ...elem,
      uri
    };
  });

  return (
    <View style={styles.container}>
      <FlatList
        refreshControl={
          <RefreshControl refreshing={!data} onRefresh={onRefresh} />
        }
        renderItem={renderItem}
        data={dataWithUris}
        keyExtractor={keyExtractor}
        getItemLayout={getItemLayout}
        windowSize={20}
        initialNumToRender={12}
        removeClippedSubviews
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 12
  },
  emptyStateContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    backgroundColor: "white",
    paddingBottom: 60
  },
  emptyStateImage: {
    width: 100,
    height: 100
  }
});

export default PictureList;
