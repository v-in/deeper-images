import React from "react";
import { View, Text, Image } from "react-native";
import { EvilIcons } from "@expo/vector-icons";
import { iOSUIKit, iOSColors } from "react-native-typography";
import { format } from "timeago.js";
import { useNavigation } from "@react-navigation/core";
import { TouchableOpacity } from "react-native-gesture-handler";

export const IMAGE_DIMENSIONS = 45;
export const TOTAL_PICTURE_ROW_HEIGHT = 66;

const PictureItem = ({ fileName, uri, size, createdAt }) => {
  const navigation = useNavigation();

  const onPress = () => navigation.navigate("View Picture", { uri });

  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.pictureContainer}>
          <Image
            style={styles.image}
            source={{
              uri: uri
            }}
          />
        </View>
        <View style={styles.textContainer}>
          <View>
            <Text style={iOSUIKit.body}>{fileName}</Text>
            <Text style={[iOSUIKit.footnoteObject, { color: iOSColors.gray }]}>
              {`${format(createdAt)} - ${size.toFixed(2)} mb`}
            </Text>
          </View>
          <EvilIcons size={30} color="grey" name="chevron-right" />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = {
  container: {
    alignItems: "stretch",
    flexDirection: "row",
    backgroundColor: "white",
    height: TOTAL_PICTURE_ROW_HEIGHT
  },
  pictureContainer: {
    width: IMAGE_DIMENSIONS,
    height: IMAGE_DIMENSIONS,
    backgroundColor: "white",
    marginVertical: 8,
    marginHorizontal: 12
  },
  textContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    borderBottomColor: "rgba(0,0,0,0.1)",
    borderBottomWidth: 1
  },
  image: {
    width: IMAGE_DIMENSIONS,
    height: IMAGE_DIMENSIONS,
    borderRadius: 2,
    resizeMode: "contain"
  }
};

export default React.memo(PictureItem);
