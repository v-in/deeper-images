import React, { useState, useLayoutEffect, createRef, useRef } from "react";
import { StyleSheet, View, Dimensions, Text } from "react-native";
import useCamera from "../../hooks/useCamera";
import { TextInputRow, NavigationInputRow } from "../../components/InputRow";
import ConfirmButton from "./ConfirmButton";

const NewPicture = ({ navigation }) => {
  const inputRef = useRef(createRef());
  const [name, setName] = useState("");
  const [fileUri, setFileUri] = useState("");
  const { takePicture } = useCamera();

  const action = () => {
    takePicture().then(result => {
      if (!result.cancelled) {
        setFileUri(result.uri);
        inputRef.current.focus();
      }
    });
  };

  const dirs = fileUri.split("/");
  const filename = dirs[dirs.length - 1];

  const updateFilename = newText => {
    // Dont allow spaces
    if (!newText.includes(" ")) setName(newText);
  };

  // Adding confirmation button here because it needs to access screen state.
  // see https://reactnavigation.org/docs/header-buttons/#header-interaction-with-its-screen-component
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <ConfirmButton
          fileUri={fileUri}
          filename={name}
          disabled={fileUri.length === 0 || name.length === 0}
        />
      )
    });
  }, [navigation, fileUri, name]);

  return (
    <View style={styles.container}>
      <NavigationInputRow
        title="File*"
        action={action}
        info={fileUri.length > 0 ? filename : "take a picture"}
      />
      <TextInputRow
        ref={inputRef}
        title="Save as*"
        placeholder="pick a name"
        text={name}
        onChangeText={updateFilename}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    paddingTop: 20
  },
  previewContainer: {
    paddingLeft: 12,
    paddingTop: 12
  },
  imagePreviewContainer: {
    paddingTop: 12,
    justifyContent: "center",
    flexDirection: "row"
  }
});

export default NewPicture;
