import React, { useState } from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ActivityIndicator, Text } from "react-native";
import { iOSColors, iOSUIKit } from "react-native-typography";
import useMultipartImageUpload from "../../hooks/useMultipartImageUpload";
import { API_URL } from "../../constants";
import { useNavigation } from "@react-navigation/core";
import { mutate } from "swr";

const ConfirmButton = ({ fileUri, disabled, filename }) => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const { startUpload } = useMultipartImageUpload({
    onComplete: () => setLoading(false)
  });

  const onPress = () => {
    setLoading(true);
    startUpload(API_URL + "/images", fileUri, filename)
      .then(() => {
        setLoading(false);
        mutate("/images");
        navigation.navigate("Pictures");
      })
      .catch(err => {
        setLoading(false);
        alert("Error while trying to upload photo!");
      });
  };

  if (loading) return <ActivityIndicator color={iOSColors.blue} />;

  return (
    <TouchableOpacity onPress={onPress} disabled={disabled}>
      <Text
        style={{
          ...iOSUIKit.bodyEmphasized,
          color: disabled ? iOSColors.gray : iOSColors.blue
        }}
      >
        Upload
      </Text>
    </TouchableOpacity>
  );
};

export default ConfirmButton;
