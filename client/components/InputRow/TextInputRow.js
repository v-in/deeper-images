import React, { useRef } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  StyleSheet
} from "react-native";
import { iOSUIKit, iOSColors } from "react-native-typography";

const TextInputRow = React.forwardRef(
  ({ title, placeholder, action, disabled, text, onChangeText }, ref) => {
    const inputRef = useRef(React.createRef());

    const onContainerPressed = () => {
      if (action !== undefined) action();
      else inputRef.current.focus();
    };

    return (
      <TouchableOpacity onPress={onContainerPressed} disabled={disabled}>
        <View style={styles.container}>
          <View>
            <Text
              style={{
                ...styles.title,

                color: disabled ? iOSColors.gray : "black"
              }}
            >
              {title}
            </Text>
          </View>
          <View style={styles.contentContainer}>
            <View>
              <TextInput
                ref={ref || inputRef}
                value={text}
                placeholder={placeholder}
                editable={!disabled}
                onChangeText={onChangeText}
                placeholderTextColor={iOSColors.gray}
                style={{ fontSize: iOSUIKit.callout.fontSize, flexGrow: 1 }}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginLeft: 12,
    paddingRight: 12,
    paddingVertical: 4,
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: iOSColors.lightGray
  },
  title: {
    ...iOSUIKit.bodyObject
  },
  contentContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center"
  }
});

export default TextInputRow;
