import React from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";
import { iOSUIKit, iOSColors } from "react-native-typography";
import { EvilIcons } from "@expo/vector-icons";

const NavigationInputRow = ({ title, action, info, disabled }) => {
  const onContainerPressed = () => {
    action();
  };

  return (
    <TouchableOpacity onPress={onContainerPressed} disabled={disabled}>
      <View style={styles.container}>
        <View>
          <Text
            style={{
              ...styles.title,

              color: disabled && iOSColors.gray
            }}
          >
            {title}
          </Text>
        </View>
        <View style={styles.contentContainer}>
          <View style={{ flexDirection: "row" }}>
            <Text style={{ ...iOSUIKit.calloutObject, color: iOSColors.gray }}>
              {info}
            </Text>
            <EvilIcons name="chevron-right" size={30} color={iOSColors.gray} />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginLeft: 12,
    paddingRight: 12,
    paddingVertical: 4,
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: iOSColors.lightGray
  },
  title: {
    ...iOSUIKit.bodyObject
  },
  contentContainer: {
    flexBasis: 100,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center"
  }
});

export default NavigationInputRow;
